﻿/* Script PlayerMovement.cs
 * written by Antonio Cordeiro
 * Feb. 2014
 * 3GB3 Assignment 02
 */
using UnityEngine;
using System.Collections;

[RequireComponent (typeof (CharacterController))]
public class CameraMovement : MonoBehaviour {
	
	public Transform targetObject; //: Transform;
	public float backDistance = 3.0f;
	public float upDistance = 1.0f;
	public float limitDistance = 4.0f;
	public float xSpeed = 1.0f;
	public float ySpeed = 1.0f;
	public float yMinClamp = -50.0f;
	public float yMaxClamp = 50.0f;
	public float followFactor = 5.0f;
	public float rotateFactor = 5.0f;
	public bool canMoveTo = true;
	public bool canLookAt = true;
	public bool canRotate = true;
	public LayerMask mask = -1;
	public Vector3 offset = Vector3.zero; //Vector3(0.0f, 0.5f, 0.0f);
	
	// Private controllers
	private float x;
	private float y;
	private float xAxisIntensity = 2.0f;
	private float yAxisIntensity = 2.0f;
	private bool isAnimating = false;
	private CameraDefaults cameraDefaults;
	
	void Start () {
		// Start addressing angles
		x = transform.eulerAngles.x;
		y = transform.eulerAngles.y;
		
		cameraDefaults = new CameraDefaults();
		cameraDefaults.backDistance = this.backDistance;
		cameraDefaults.upDistance = this.upDistance;
		cameraDefaults.limitDistance = this.limitDistance;
		cameraDefaults.xSpeed = this.xSpeed;
		cameraDefaults.ySpeed = this.ySpeed;
		cameraDefaults.yMinClamp = this.yMinClamp;
		cameraDefaults.yMaxClamp = this.yMaxClamp;
		
	}
	
	// Update is called once per frame
	void LateUpdate () {
		Debug.DrawRay(this.transform.position, this.transform.forward, Color.magenta);
		
		// Checking existence of transform
		if (targetObject && !isAnimating){
			// Horizontal and vertical axis values
			float horizontal = Input.GetAxis("Mouse X");// || Input.GetAxis("HorizontalRight");
			float vertical = Input.GetAxis("Mouse Y");// || Input.GetAxis("VerticalRight");
			
			horizontal = Mathf.Clamp(horizontal, -xAxisIntensity, xAxisIntensity);
			vertical = Mathf.Clamp(vertical, -yAxisIntensity, yAxisIntensity);
			
			// Rotate angles based upon axis speed
			if (canRotate){
				x += horizontal * xSpeed;
				y += vertical * ySpeed;
			}
			
			// Clamp axis maxium rotation
			y = Mathf.Clamp (y, yMinClamp, yMaxClamp);
			
			// Perform the rotation using quaternions
			Vector3 relativeDistance = (targetObject.position + offset) - transform.position;
			Quaternion toRotation = Quaternion.LookRotation (relativeDistance);
			
			// Calculate distances from target
			Vector3 toDistance = new Vector3(0.0f, upDistance, -backDistance);
			Vector3 toPosition = Quaternion.Euler (-y, x, 0) * toDistance + targetObject.position;
			
			// Limit distance reached?
			if (Vector3.Distance(transform.position, toPosition) > limitDistance){
				transform.position = toPosition;
			}
			
			toPosition = CompensateWallHit(toPosition);
			
			// Apply to transform
			if (canMoveTo) transform.position = Vector3.Lerp (transform.position, toPosition, Time.deltaTime * followFactor);
			if (canLookAt) transform.rotation = Quaternion.Lerp (transform.rotation, toRotation, Time.deltaTime * rotateFactor);
		}
	}
	
	
	// Detects and takes action upon wall colision
	Vector3 CompensateWallHit(Vector3 toPosition){
		RaycastHit hit;// : RaycastHit;
		if (Physics.Linecast (this.transform.position, toPosition, out hit, mask)) 
			toPosition = hit.point + hit.normal * 0.3f;
		
		return toPosition;
	}
	
	// Animation purposes
	void StartAnimation(){
		isAnimating = true;
	}
	
	void EndAnimation(){
		isAnimating = false;
	}
	
	// Camera defaults
	class CameraDefaults{
		public float backDistance;
		public float upDistance;
		public float limitDistance;
		public float xSpeed;
		public float ySpeed;
		public float yMinClamp;
		public float yMaxClamp;
	}
	
	// Restore defaults
	void restoreCameraSettings(){
		this.backDistance = this.cameraDefaults.backDistance;
		this.upDistance = this.cameraDefaults.upDistance;
		this.limitDistance = this.cameraDefaults.limitDistance;
		this.xSpeed = this.cameraDefaults.xSpeed;
		this.ySpeed = this.cameraDefaults.ySpeed;
		this.yMinClamp = this.cameraDefaults.yMinClamp;
		this.yMaxClamp = this.cameraDefaults.yMaxClamp;
		
	}
}