﻿using UnityEngine;
using System.Collections;

public class UnescapeNewline : MonoBehaviour {

	// Use this for initialization
	void Start () {
		TextMesh textMeshReference = GetComponent<TextMesh> ();
		textMeshReference.text = textMeshReference.text.Replace ("\\n", "\n");
	}
}
