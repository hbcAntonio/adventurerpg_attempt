using UnityEngine;
using System.Collections;

public class ItemEffect : MonoBehaviour
{

	public enum ItemId{

		NullItemId = -1,
		SmallGreenHealthPotion,
	}

	public void callItemEffect(ItemId itemid){
	
		// Huge if-else thing

		switch (itemid) {
		
			case ItemId.SmallGreenHealthPotion:

			SmallHealthPotion item = new SmallHealthPotion();
			item.Execute();

			break;

			default:
			Debug.Log ("Called default effect!");
			break;
		}
	}
}

