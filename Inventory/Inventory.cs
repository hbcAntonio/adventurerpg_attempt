﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Inventory : MonoBehaviour {

	public static List<ItemSpace> itemSpaces;
	private GameObject[] _itemSpaces;

	void Start(){

		itemSpaces = new List<ItemSpace>();
		_itemSpaces = GameObject.FindGameObjectsWithTag ("ItemSpace");

		foreach(GameObject itemSpace in _itemSpaces){
			itemSpaces.Add (itemSpace.GetComponent<ItemSpace>());
		}

		/*Debug.Log (itemSpaces.Count);

		foreach (ItemSpace itemSpace in itemSpaces) {
			Debug.Log (itemSpace.index);		
		}*/
	}

	public static bool collectItem(GameManager.Item itemReference){
		int itemIndex = GameManager.getItemIndex(itemReference.itemName);
		
		// If item exists in list, add quantity, else add reference
		if (itemIndex == -1){
			GameManager.itemList.Add(itemReference);
			int lastItemIndex = GameManager.itemList.Count - 1;
			int freeSlotIndex = getFreeSpaceIndex();

			if (freeSlotIndex != -1){
				itemSpaces[freeSlotIndex].updateItemIndex(lastItemIndex);
				return true;
			}
			else{
				Debug.Log ("INVENTORY FULL!");
				return false;
			}

		}
		else {
			GameManager.itemList[itemIndex].itemQuantity += itemReference.itemQuantity;
			return true;
		}
	}

	public static int getFreeSpaceIndex(){
		for(int i = 0; i < itemSpaces.Count; ++i){
			if (itemSpaces[i].index == -1) return i;
		}

		return -1;
	}

	public static void updateItemIndexesForRemoved(int indexRemoved){

		Debug.Log ("updateItemIndexesForRemoved called with indexRemoved being: " + indexRemoved.ToString ());
		foreach (ItemSpace itemSpace in itemSpaces){

			if (itemSpace.index > indexRemoved){

				Debug.Log ("This index is greater than the one being removed. " + itemSpace.index.ToString() + " should now be " + (itemSpace.index-1).ToString());
				//itemSpace.index -= 1;
				itemSpace.updateItemIndex(itemSpace.index-1);
			} else if (itemSpace.index == indexRemoved) {
				itemSpace.updateItemIndex(-1);
			
			}

		}
	}


}
