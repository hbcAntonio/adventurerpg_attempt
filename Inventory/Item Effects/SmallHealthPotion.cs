using UnityEngine;
using System.Collections;

public class SmallHealthPotion : MonoBehaviour
{

	private GameObject player;
	private DamageHandler playerDamageHandler;

	void Start(){
		player = GameObject.FindGameObjectWithTag ("Player");
		playerDamageHandler = player.GetComponent<DamageHandler> ();
	}

	public void Execute(){
		playerDamageHandler.health += playerDamageHandler.maxHealth * 0.3f;
		if (playerDamageHandler.health > playerDamageHandler.maxHealth) playerDamageHandler.health = playerDamageHandler.maxHealth;
	}
}

