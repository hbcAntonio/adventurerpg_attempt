using UnityEngine;
using System.Collections;

public class ItemDetail : MonoBehaviour
{
	public static TextMesh itemName;
	public static TextMesh itemDescription;
	public static GameObject itemIcon;
	public static int itemIndex;

	public TextMesh _itemName;
	public TextMesh _itemDescription;
	public GameObject _itemIcon;

	void Awake(){
		if (itemName == null) itemName = _itemName;
		if (itemDescription == null) itemDescription = _itemDescription;
		if (itemIcon == null) itemIcon = _itemIcon;
	}

	public static void setItemDetailInfo(int index){
		itemIndex = index;
		itemName.text = GameManager.itemList[index].itemName + " (X" + GameManager.itemList[index].itemQuantity.ToString() + ")";
		itemDescription.text = GameManager.itemList[index].itemDescription.Replace ("\\n", "\n");
		itemIcon.GetComponent<Renderer>().material = GameManager.itemList[index].itemMaterial;
	}
}

