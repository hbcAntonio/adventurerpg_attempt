﻿using UnityEngine;
using System.Collections;

public class ItemSpace : MonoBehaviour {

	[HideInInspector]
	public int index = -1;

	// Is item a backgorund space
	public bool isBackgroundSpace;

	// Is mouse over item
	[HideInInspector]
	public bool isMouseOver;

	// Initial scale
	private Vector3 initialScale;

	void Awake(){
		// If item is not a background space, set index
		if (!isBackgroundSpace) {
			index = -1;
			initialScale = transform.localScale;
			transform.localScale = Vector3.zero;
		}
	}

	void Update(){
		if (Input.GetMouseButtonDown (0)) {
			if (isMouseOver && index != -1 && GetComponent<Renderer>().enabled && GameManager.gameGUI.currentWindow == GameGUI.GUIWindowId.InventoryWindow){
				if (isBackgroundSpace) {
				
				} else {
					
					// It is not background space, so translate click into
					// opening the item detail scene passing the correct
					// item index
					
					if (GameManager.gameGUI.allowedToCallGUI){
						Debug.Log (index);
						GameManager.gameGUI.setDisplayWindow(GameGUI.GUIWindowId.ItemDetailWindow, true);
						ItemDetail.setItemDetailInfo(index);
						
					}
					//;
					
				}
			}
		}
	}

	/* This function will only be called for those item spaces that
	 * are not background spaces, i.e spaces that are not untagged */
	public void updateItemIndex(int index){
		Debug.Log ("Index " + index.ToString () + " received.");
		this.index = index;
		if (index != -1) this.GetComponent<Renderer>().material = GameManager.itemList [index].itemMaterial;
		this.transform.localScale = index == -1 ? Vector3.zero : initialScale;
	}

	/* Mouse over functions for background and item spaces */
	void OnTriggerEnter(Collider other){
		if (!other.CompareTag("MouseBlock")) return;
		//Debug.Log ("Mouse entered!");
		isMouseOver = true;

		if (isBackgroundSpace) {

			GetComponent<Renderer>().material = GameManager.empty_item_bg_mouseover_material;

		} else {
			
		}
	}

	void OnTriggerExit(Collider other){
		if (!other.CompareTag("MouseBlock")) return;
		isMouseOver = false;

		if (isBackgroundSpace) {
			GetComponent<Renderer>().material = GameManager.empty_item_bg_material;

		} else {
		
		}
	}
}
