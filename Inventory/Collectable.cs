﻿using UnityEngine;
using System.Collections;

public class Collectable : MonoBehaviour {

	public GameManager.Item itemReference;
	public bool playerIsInside;// = false;
	
	public AnimationClip scaleUp;
	public AnimationClip scaleDown;

	public Animation interactAnimation;
	public Animation itemNameTextAnimation;
	public TextMesh itemNameTextMesh;

	// Use this for initialization
	void Start () {
		playerIsInside = false;

		// Item name and quantity in format ItemName(xQuantity)
		itemNameTextMesh.text = itemReference.itemName + "(x" + itemReference.itemQuantity + ")";

		interactAnimation.transform.localScale = Vector3.zero;
		itemNameTextAnimation.transform.localScale = Vector3.zero;

		// Attach item to this prefab

	}
	
	// Update is called once per frame
	void Update () {
	
		if (playerIsInside) {

			if (Input.GetKeyDown(KeyCode.E)){

				// Call collect function and assigns collect variable
				bool collected = Inventory.collectItem(itemReference);

				// If collected 
				if (collected){

					interactAnimation.Play (scaleDown.name);
					itemNameTextAnimation.Play (scaleDown.name);
					Destroy (this.gameObject);

				} else {
					// Do animation for Inventory Full thingy
				}
			}
		}
	}

	void OnTriggerEnter(Collider other){

		// Player is inside area
		if (!other.CompareTag("Player")) return;
		playerIsInside = true;
		interactAnimation.Play (scaleUp.name);
		itemNameTextAnimation.Play (scaleUp.name);

	}

	void OnTriggerExit(Collider other){

		// Player is not inside area
		if (!other.CompareTag("Player")) return;
		playerIsInside = false;
		interactAnimation.Play (scaleDown.name);
		itemNameTextAnimation.Play (scaleDown.name);

	}
}
