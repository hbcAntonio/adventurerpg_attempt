﻿using UnityEngine;
using System.Collections;

public class CopyParentText : MonoBehaviour {

	private TextMesh parentTextMesh;
	private TextMesh thisTextMesh;

	void Start(){
		thisTextMesh = GetComponent<TextMesh> ();
		parentTextMesh = transform.parent.GetComponent<TextMesh> ();
	}

	void Update(){
		thisTextMesh.text = parentTextMesh.text;
	}
}
