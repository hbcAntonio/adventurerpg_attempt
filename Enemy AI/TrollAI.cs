﻿using UnityEngine;
using System.Collections;

public class TrollAI : MonoBehaviour {

	// Player
	public Transform player;
	public float distanceTrigger = 5.0f;
	public float attackDistance = 1.0f;
	public float runningDisrance = 3.5f;
	public float walkSpeed = 1.2f;
	public float runSpeed = 3.4f;
	public float enemyXP;

	/*public AnimationClip idleAnimation;
	public AnimationClip walkAnimation;
	public AnimationClip runAnimation;
	public AnimationClip attackAnimation;*/

	// Enemy AI state
	private enum EnemyStates {Idle, Seeking, Attacking, TakingDamage, Dead};
	private EnemyStates enemyState;
	private NavMeshAgent nav;
	private Animator anim;
	private DamageHandler damageHandler;

	void Start () {
		// Get vital components
		nav = GetComponent<NavMeshAgent>();
		anim = GetComponent<Animator> ();
		damageHandler = GetComponent<DamageHandler> ();

		// Enemy starts idling
		enemyState = EnemyStates.Idle;
	}

	void Update(){


		// If enemy is dead, do nothing
		if (enemyState == EnemyStates.Dead) return;

		// If player is dead, just assume idle state
		if (PlayerMovement.IsPlayerDead()) enemyState = EnemyStates.Idle;

		// Kill troll
		if (damageHandler.health <= 0.0f){
			nav.velocity = Vector3.zero;
			enemyState = EnemyStates.Dead;

			// Give player xp
			XPSytem.AddExp (enemyXP);
		}

		// Idle
		if (enemyState == EnemyStates.Idle){

			// If player is within minimun distance, seek player
			if (IsPlayerNear (distanceTrigger)) {
				//Debug.Log("Triggered");
				enemyState = EnemyStates.Seeking;
			}

		} else if (enemyState == EnemyStates.Seeking){

			//Debug.Log ("Seeking");
			nav.SetDestination(player.position);

			if (IsPlayerNear (runningDisrance)) nav.speed = walkSpeed;
			else nav.speed = runSpeed;

			if (!IsPlayerNear(distanceTrigger)) enemyState = EnemyStates.Idle;
			if (IsPlayerNear (attackDistance)) enemyState = EnemyStates.Attacking;

		} else if (enemyState == EnemyStates.Attacking){

			nav.velocity = Vector3.zero;
			if (!IsPlayerNear(attackDistance)){
				doneAttacking();
				enemyState = EnemyStates.Seeking;
			}

		} else if (enemyState == EnemyStates.TakingDamage){

			// Do not move when being hit (bake into pose must be checked)
			nav.velocity = Vector3.zero;

			// Once is done taking the hit, go back to attacking
			if (!anim.GetBool("Hit")) enemyState = EnemyStates.Attacking;
		}

		// Is enemy taking damage?
		if (damageHandler.HasTakenDamage()){

			// Acknowledge that damage was taken
			damageHandler.ResetDamageTaken();

			// Prep
			anim.SetBool("Hit", true);
			anim.SetBool("Attacking", false);
			enemyState = EnemyStates.TakingDamage;

			//transform.LookAt(player.transform);
		}

		// Handle animation
		Animate ();

	
	}

	/* Is player near enough ? */
	bool IsPlayerNear(float dist){
		if (PlayerMovement.IsPlayerDead()) return false;

		if (Vector3.Distance(transform.position, player.position) < dist) return true;
		else return false;
	}

	void Animate(){
		// Update animator states
		anim.SetFloat ("Distance", Vector3.Distance (transform.position, player.position));
		anim.SetFloat ("Speed", nav.velocity.magnitude);

		if (enemyState == EnemyStates.Attacking){
			anim.SetBool("Attacking", true);
		}

		if (enemyState == EnemyStates.TakingDamage){
			anim.SetBool("Hit", true);
		}

		if (enemyState == EnemyStates.Dead) {
			doneAttacking();
			doneHit();
			anim.SetBool ("Dead", true);
		}

	}

	/* Enemy hit */
	void doneHit(){
		anim.SetBool ("Hit", false);
	}
	
	/* Attack events */
	void lookAtPlayer(){
		transform.LookAt(player.transform);
	}

	void doneAttacking(){
		anim.SetBool ("Attacking", false);
	}

	/* Deactivate collider after death */
	void DeactivateCollider(){
		GetComponent<Collider>().enabled = false;
	}
} 