﻿/* Script PlayerMovement.cs
 * written by Antonio Cordeiro
 * Feb. 2014
 * 3GB3 Assignment 02
 */
using UnityEngine;
using System.Collections;

[RequireComponent (typeof (CharacterController))]
public class PlayerMovement : MonoBehaviour {

	// Camera orientation
	public Camera mainCamera;

	// Public attributes
	public float walkSpeed = 2.0f;
	public float runSpeed = 4.0f;
	public float rotateSpeed = 5.0f;
	public float jumpSpeed = 5.0f;
	public float gravity = 28.0f;

	// Control
	public bool canRun = true;
	public bool canJump = true;

	// Debug
	public bool isOnDebug = false;
	private bool isImmortal = false;

	// Animation
	public AnimationClip idleAnimation;
	public AnimationClip walkAnimation;
	public AnimationClip runAnimation;
	public AnimationClip hitAnimation;
	public AnimationClip attackAnimation;
	public AnimationClip dieAnimation;

	// Components
	private CharacterController controller;
	private DamageHandler damageHandler;
	private Vector3 toPosition = Vector3.zero;
	private Vector3 gravityVector = Vector3.zero;

	// States
	private bool isMoving = false;
	private bool isRunning = false;
	private bool isJumping = false;
	private bool isBeingHit = false;

	// Must be known to other scripts
	static bool isAttacking = false;
	static bool isDead = false;

	/* Initialises all components */
	void Start () {
		controller = GetComponent<CharacterController> ();
		damageHandler = GetComponent<DamageHandler> ();
		mainCamera = GameManager.mainCamera.GetComponent<Camera>();
	}
	
	/* Calculates everything needed to move the player */
	void Update () {
	
		// Debug?
		if (isOnDebug) DebugPlayer();

		// If player isn't dead nor being hit, calculate movement
		if (!isDead && !isBeingHit){

			// Axis input
			float horizontal = Input.GetAxis ("Horizontal");
			float vertical = Input.GetAxis ("Vertical");

			// Check movement
			if (new Vector2(horizontal, vertical).magnitude != 0.0f) isMoving = true;
			else isMoving = false;

			// If is moving, perform calculations
			if (isMoving && !isAttacking){

				// Camera vectors
				Vector3 camRight = mainCamera.transform.right;
				Vector3 camForward = mainCamera.transform.forward;

				// Kill Y for orientation
				camRight.y = camForward.y = 0.0f;

				// Move direction
				toPosition = camForward.normalized * vertical + camRight.normalized * horizontal;

				// Character running?
				if ((Input.GetAxis("RunJoystick") != 0 || Input.GetAxis("RunKeyboard") != 0) && canRun) isRunning = true;
				else isRunning = false;


				// Calculate speed
				float speed = isRunning ? runSpeed : walkSpeed;
				toPosition *= speed;

				// Calculate rotation needed
				Vector3 relativeDistance = new Vector3(toPosition.x + transform.position.x, transform.position.y, toPosition.z + transform.position.z) - transform.position;
				if (relativeDistance.magnitude <= 0.0f) return;

				// Rotate controller
				Quaternion toRotation = Quaternion.LookRotation(relativeDistance);
				transform.rotation = Quaternion.Lerp(transform.rotation, toRotation, Time.deltaTime * rotateSpeed);

			}

			// Character jump is independent
			if (Input.GetButtonDown ("Jump") && canJump && controller.isGrounded) {
				//toPosition.y = jumpSpeed;
				gravityVector.y = jumpSpeed;
			}

			// Character attack is independent
			if (Input.GetButton ("Attack")) {
				isAttacking = true;
			}
			else {
				isAttacking = false;
			}
		}

		// Apply final considerations
		if (isAttacking) isMoving = false;
		if ((!isMoving && controller.isGrounded) || isDead) toPosition.x = toPosition.z = 0.0f;

		// Apply gravity and move controller
		if (!controller.isGrounded) gravityVector.y -= gravity * Time.deltaTime;
		controller.Move ((toPosition + gravityVector) * Time.deltaTime);

		// At this point, nothing matters if player is dead
		if (isDead) return;

		// Handles animation
		Animate ();

		// Handles player life
		if (damageHandler.HasTakenDamage ()) {

			// Stops attacking and moving
			isAttacking = false;
			isMoving = false;

			isBeingHit = true;
			GetComponent<Animation>().CrossFade(hitAnimation.name);

			// Acknowledge damage
			damageHandler.ResetDamageTaken();

			// Check if player should be alive
			if (damageHandler.health <= 0.0f && !isImmortal){
				isDead = true;
				isAttacking = false;
				GetComponent<Animation>().CrossFade(dieAnimation.name);
			}
		}

		// Is player still being hit?
		if (isBeingHit && !GetComponent<Animation>().IsPlaying(hitAnimation.name)) isBeingHit = false;
	}

	/* Handles animation transitions and playback */
	void Animate(){

		// Do nothing if player is dead
		if (isDead) return;

		if (isMoving){
			
			if (isRunning) checkAnimationAndCrossFade(runAnimation);
			else checkAnimationAndCrossFade(walkAnimation);

		} else if (isAttacking) {

			checkAnimationAndCrossFade(attackAnimation);

		//} else if (isBeingHit) {

		//	checkAnimationAndCrossFade(hitAnimation);

		} else {

			checkAnimationAndCrossFade (idleAnimation);
		}

	}


	/* Player hit controller */
	void PlayerHit(bool hit){
		if (isDead) return;

		if (hit){
			isAttacking = false;
			isMoving = false;


		}

		isBeingHit = hit;
	}

	/* Animation (legacy) support functions */
	void checkAnimationAndPlay(AnimationClip anim){
		if (!GetComponent<Animation>().IsPlaying(anim.name))
			GetComponent<Animation>().Play (anim.name);
	}

	void checkAnimationAndCrossFade(AnimationClip anim){
		if (!GetComponent<Animation>().IsPlaying(anim.name))
			GetComponent<Animation>().CrossFade (anim.name);
	}

	void checkAnimationAndBlend(AnimationClip anim){
		if (!GetComponent<Animation>().IsPlaying(anim.name))
			GetComponent<Animation>().Blend(anim.name);
	}

	void checkAnimationAndStop(AnimationClip anim){
		if (!GetComponent<Animation>().IsPlaying(anim.name))
			GetComponent<Animation>().Stop(anim.name);
	}

	/* Static functions */
	public static bool IsPlayerAttacking(){
		return isAttacking;
	}

	public static bool IsPlayerDead(){
		return isDead;
	}

	/* Debug function */
	void DebugPlayer(){
		// Debugging commands
		//if (Input.GetKeyDown (KeyCode.LeftShift)){
			
			//Debug.Log ("Here");
			// Revive
			if (Input.GetKeyDown (KeyCode.R)){
				damageHandler.health = 100.0f;
				damageHandler.ResetDamageTaken();
				isDead = false;
				Debug.LogWarning("Player revived");
			} else if (Input.GetKeyDown (KeyCode.I)){
				isImmortal = !isImmortal;
				Debug.LogWarning("Player immortal: " + isImmortal);
			}
		//}
	}
}
