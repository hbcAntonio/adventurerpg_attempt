﻿using UnityEngine;
using System.Collections;

public class DamageDealer : MonoBehaviour {

	// Minimun and maximun damage
	public float minDamage;
	public float maxDamage;

	// Critical attack
	public float critChance;
	public float critBoost;

	// Miss rate
	public float missChance;

	// Attack speed
	public float attackSpeed = .5f;

	// Controllers
	public bool useRandomBetweenMinAndMax;
	public bool isPlayer;
	public bool isSpecial;
	public bool isHitKill;

	// Allowed to attack following tags
	public string[] tagList;

	// Timer controller
	private bool allowedToDealDamage = true;

	// Debug
	public bool debug = false;

	void OnTriggerStay(Collider other){
		//if (debug) Debug.Log (gameObject.name + " damageDealer collided with " + other.tag);
		if (isPlayer && !PlayerMovement.IsPlayerAttacking()) return;
		if (!allowedToDealDamage) return;

		bool hasTag = false;
		for (int i = 0; i < tagList.Length; ++i) {
			if (other.CompareTag(tagList[i])){
				hasTag = true;
				Debug.Log ("Has tag: " + other.tag);
				break;
			}
		}

		if (!hasTag) return;

		DamageHandler handler = other.GetComponent<DamageHandler> ();
		if(!handler) return;

		// Calculate damage
		float damage = useRandomBetweenMinAndMax ? Random.Range (minDamage, maxDamage) : minDamage;

		bool applyCrit = Random.Range (1, 100) <= critChance ? true : false;
		bool miss = Random.Range (1, 100) <= missChance ? true : false;

		if (!miss) damage = applyCrit ? damage * critBoost : damage;
		else damage = 0.0f;

		// Calculate damage type
		DamageHandler.DamageTypes type;

		if (!isHitKill){

			// If damage type is not hit kill, calculate type
			if (damage < 0.0f)
				type = DamageHandler.DamageTypes.HEAL;
			else if (damage == 0.0f)
				type = DamageHandler.DamageTypes.MISS;
			else if (damage > 0.0f && isSpecial)
				type = DamageHandler.DamageTypes.SPECIAL;
			else if (applyCrit)
				type = DamageHandler.DamageTypes.CRITICAL;
			else if (isHitKill)
				type = DamageHandler.DamageTypes.HITKILL;
			else
				type = DamageHandler.DamageTypes.NORMAL;
		} else {

			// Type is hit kill
			type = DamageHandler.DamageTypes.HITKILL;
		}

		// Apply damage
		handler.TakeDamage(damage ,type);
		StartCoroutine (resetDamageTimer ());
		allowedToDealDamage = false;
	}

	void OnTriggerExit(Collider other){
		//Debug.Log ("Object " + gameObject.name + " colliding with " + other.name);
	}

	void OnTriggerEnter(Collider other){
		//Debug.Log ("Object " + gameObject.name + " NOT colliding with " + other.name);
	}

	IEnumerator resetDamageTimer(){
		yield return new WaitForSeconds(attackSpeed);
		allowedToDealDamage = true;
	}
}
