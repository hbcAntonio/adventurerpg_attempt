﻿using UnityEngine;
using System.Collections;

public class CrystalBuff : MonoBehaviour {

	public DamageDealer[] dealersToBuff;
	public DamageHandler playerHealth;
	public GameObject particleBuffDealer;

	public float newMinAttackValue = 10000.0f;
	public float newMaxAttackValue = 10000.0f;
	public float newHealthMax = 100000.0f;
	public float buffDuration = 10.0f;

	public ParticleSystem thisParticleSystemEffect;

	private DamageHandler handler;
	private bool buffGiven = false;
	private GameObject[] buffParticles;
	private float[] dealersOldValuesMax;
	private float[] dealersOldValuesMin;
	private float healthOldValue;

	void Start () {
		// Damage Handler for the crystal
		handler = GetComponent<DamageHandler> ();
	}

	void Update () {

		// Buff has already been given
		if (buffGiven) return;

		// Still not destroyed
		if (handler.health > 0.0f) return;

		// Proceed
		buffParticles = new GameObject[dealersToBuff.Length];
		dealersOldValuesMin = new float[dealersToBuff.Length];
		dealersOldValuesMax = new float[dealersToBuff.Length];

		for (int i = 0; i < dealersToBuff.Length; ++i){
			dealersOldValuesMin[i] = dealersToBuff[i].minDamage;
			dealersOldValuesMax[i] = dealersToBuff[i].maxDamage;

			dealersToBuff[i].minDamage = newMinAttackValue;
			dealersToBuff[i].maxDamage = newMaxAttackValue;

			buffParticles[i] = Instantiate(particleBuffDealer, Vector3.zero, Quaternion.identity) as GameObject;
			buffParticles[i].transform.parent = dealersToBuff[i].transform;
			buffParticles[i].transform.localPosition = Vector3.zero;
			buffParticles[i].transform.localRotation = buffParticles[i].transform.parent.localRotation;
		}

		// Health buff
		healthOldValue = playerHealth.maxHealth;
		playerHealth.maxHealth = newHealthMax;
		playerHealth.health = playerHealth.maxHealth;

		// Stop effect
		thisParticleSystemEffect.Stop ();
		buffGiven = true;

		// Start coroutine to end buff
		StartCoroutine (cancelBuff ());
	}

	IEnumerator cancelBuff(){
		yield return new WaitForSeconds(buffDuration);

		for (int i = 0; i < dealersToBuff.Length; ++i){
			dealersToBuff[i].minDamage = dealersOldValuesMin[i];
			dealersToBuff[i].maxDamage = dealersOldValuesMax[i];

			Destroy(buffParticles[i]);
		}

		playerHealth.maxHealth = healthOldValue;

		buffGiven = false;
		handler.health = 1f;
		thisParticleSystemEffect.Play ();
	}
}
