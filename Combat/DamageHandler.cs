﻿using UnityEngine;
using System.Collections;

public class DamageHandler : MonoBehaviour {

	public float health;
	public float maxHealth;
	public bool destroyOnHealthZero = false;
	public bool displayBar = true;

	// Debug
	public bool debug = false;

	// Static
	public enum DamageTypes {NORMAL, CRITICAL, HEAL, MISS, SPECIAL, HITKILL}; // Found out this is static either way

	// Privates
	private float damageTaken;

	public void TakeDamage(float damage, DamageTypes damageType){

		// Animate damage
		Animate (damage, damageType);

		// Is a hit kill?
		if (damageType == DamageTypes.HITKILL){
			
			// Has taken damage
			damageTaken = 1.0f;
			
			// Health is 0
			health = 0.0f;
			
		} else {
			
			// Saves damage taken in a hit
			damageTaken = health - (health - damage);
			
			// Decrease/increase health
			health -= damage;
		}

		// Destroy if health is zero
		if (destroyOnHealthZero && health <= 0.0f) Destroy(this.gameObject);

		// Clamp health
		if (health > maxHealth) health = maxHealth;
		if (health < 0.0f) health = 0.0f;

	}

	/* Display damage animation */
	void Animate(float damage, DamageTypes damageType){

		// Only display damage if type if HEAL or HP is more than 0
		if (health > 0.0f || damageType == DamageTypes.HEAL){
			
			// Create damage text
			GameObject damageTextObj = GameObject.Instantiate (GameManager.damageConfigs.damageText, transform.position + Vector3.up * .5f, Quaternion.identity) as GameObject;
			StartCoroutine (destroyDamageText (damageTextObj));
			Debug.Log ("Suposed to have instantiated!");
			TextMesh text = damageTextObj.GetComponentInChildren<TextMesh> ();
			
			// Calculate color
			if (damageType == DamageTypes.NORMAL) text.color = GameManager.damageConfigs.normalDamageColor;
			if (damageType == DamageTypes.CRITICAL) text.color = GameManager.damageConfigs.criticalDamageColor;
			if (damageType == DamageTypes.HEAL) text.color = GameManager.damageConfigs.healDamageColor;
			if (damageType == DamageTypes.SPECIAL) text.color = GameManager.damageConfigs.specialDamageColor;
			if (damageType == DamageTypes.MISS) text.color = GameManager.damageConfigs.missDamageColor;
			if (damageType == DamageTypes.HITKILL) text.color = GameManager.damageConfigs.hitKillDamageColor;
			
			// Damage text
			string damageText = Mathf.Abs (Mathf.RoundToInt (damage)).ToString ();
			text.text = damage != 0.0f ? damageText : GameManager.damageConfigs.missDamageText;
			if (damageType == DamageTypes.HITKILL) text.text = GameManager.damageConfigs.hitKillDamageText;
			
			// Animation
			if (damageType == DamageTypes.CRITICAL) damageTextObj.GetComponent<Animation>().Play(GameManager.damageConfigs.critAnim.name);
			else damageTextObj.GetComponent<Animation>().Play (GameManager.damageConfigs.normalAnim.name);
		}
	}

	/* Auxiliary methods */
	public bool HasTakenDamage(){
		return damageTaken != 0.0f ? true : false;
	}

	public void ResetDamageTaken(){
		damageTaken = 0.0f;
	}

	/* Called to destroy the instance of the damage text */
	IEnumerator destroyDamageText(GameObject instance){
		yield return new WaitForSeconds (1.0f);
		Destroy (instance);
	}

}
