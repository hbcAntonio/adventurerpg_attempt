using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GameGUI : MonoBehaviour
{
	/* Remember to add in order of declaration so enum can act as list indexing */
	public enum GUIWindowId {InventoryWindow = 0, ItemDetailWindow, StatsWindow, SkillsWindow};

	public bool allowedToCallGUI;
	public KeyCode inventoryKey;
	public GUIWindowId currentWindow;
	public List<GUIWindowId> previousWindows;

	public class GUIWindow{
		public GUIWindowId guid;
		public bool displayWindow;
		public GameObject windowObject;

		public GUIWindow(GUIWindowId id, GameObject wobject){
			guid = id;
			displayWindow = false;
			windowObject = wobject;
			GameManager.setRendererActive(windowObject, false);
		}
	}

	public List<GUIWindow> GUIWindowsList;

	void Start (){

		// Allowed to call GUI
		allowedToCallGUI = true;

		// GUI Window List
		GUIWindowsList = new List<GUIWindow> ();

		// GUI previous windows list
		previousWindows = new List<GUIWindowId> ();

		// GUI Inventory Window
		GUIWindow inventoryWindow = new GUIWindow (GUIWindowId.InventoryWindow, GameObject.FindGameObjectWithTag ("GUIInventory"));
		GUIWindowsList.Add (inventoryWindow);

		// GUI Item detail Window
		GUIWindow itemdetailWindow = new GUIWindow (GUIWindowId.ItemDetailWindow, GameObject.FindGameObjectWithTag ("GUIItemDetail"));
		GUIWindowsList.Add (itemdetailWindow);
	}

	void Update (){

		// Perform only if allowed to call GUI
		if (allowedToCallGUI) {
			if (Input.GetKeyDown(inventoryKey)){
				Debug.Log ("Called inventory key");
				setDisplayWindow(GUIWindowId.InventoryWindow, !GUIWindowsList[(int)GUIWindowId.InventoryWindow].displayWindow);
			}
		}
	}

	/* Display or hide a specific GUI window */
	public void setDisplayWindow(GUIWindowId guiWindowId, bool display){

		if (display){
			previousWindows.Add (currentWindow);
			currentWindow = guiWindowId;
		} else {
			if(previousWindows.Count > 0) {
				currentWindow = previousWindows[previousWindows.Count-1];
				previousWindows.RemoveAt(previousWindows.Count-1);
			}
		}

		GUIWindowsList [(int)guiWindowId].displayWindow = display;
		GameManager.setRendererActive (GUIWindowsList [(int)guiWindowId].windowObject, display);

		//Debug.Log (GUIWindowsList [(int)guiWindowId].windowObject.name);
		//Debug.Log (GUIWindowsList [(int)guiWindowId].displayWindow);
	}

	/* Passes parameters into item detail */
}

