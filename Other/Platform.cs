﻿using UnityEngine;
using System.Collections;

public class Platform : MonoBehaviour {

	void OnTriggerEnter(Collider other){
		if (other.CompareTag ("Player")) {
			// Parent player to trigger
			other.transform.parent = this.transform;
		}
	}

	void OnTriggerExit(Collider other){
		if (other.CompareTag("Player")){
			// Parent player to none
			other.transform.parent = null;
		}
	}
}
