using UnityEngine;
using System.Collections;
//using System.Collections.Generic;

public class XPSytem : MonoBehaviour
{
	// XP system
	public static float currentxp;
	public static double xptonextlevel;
	public static int currentLevel;
	private static bool loaded;

	// Stats
	public enum Stats {Str = 0, Agi, Vit};
	public static int[] statsPoints;
	public static int sExpandablePoints;

	// Player main weapons references
	public static DamageDealer[] playerDamageDealers;
	public static DamageHandler playerDamageHandler;

	void Start(){
		// Declare status points list if it is null
		if (!loaded){
			statsPoints = new int[3];
			currentxp = 0.0f;
			xptonextlevel = 100.0f;
			currentLevel = 1;
			loaded = true;
		}

	}

	public static void ModifyStats(Stats statsIndex, int modifier){
		statsPoints [(int)statsIndex] += modifier;

		switch (statsIndex) {
			case Stats.Str:
				foreach (DamageDealer weapon in playerDamageDealers) {
						weapon.minDamage += weapon.minDamage * 0.02f * modifier; // For every STR point, increase 2% in minDamage
						weapon.maxDamage += weapon.maxDamage * 0.05f * modifier; // For every STR point, increase 5% in maxDamage
				}
				break;

			case Stats.Agi:
				foreach (DamageDealer weapon in playerDamageDealers) {
						weapon.attackSpeed += weapon.attackSpeed * 0.02f * modifier; // For every AGI point, increase 2% in attackSpeed
				}
				break;

			case Stats.Vit:
				foreach (DamageDealer weapon in playerDamageDealers) {
						weapon.critBoost += weapon.critBoost * 0.02f * modifier; // For every VIT point, increase 2% in critBoost
						weapon.critChance += weapon.critChance * 0.01f * modifier; // For every VIT point, increase 1% in critChance
				}
				playerDamageHandler.maxHealth += playerDamageHandler.maxHealth * 0.05f * modifier; // For every VIT point, increase max HP in 5%
				break;

			default:
					break;
		}

	}

	public static void AddExp(float expToAdd){
		Debug.Log (expToAdd);
		Debug.Log (currentLevel);
		currentxp += expToAdd;
		if (currentxp > xptonextlevel) {
			currentLevel++;
			xptonextlevel += xptonextlevel * 0.2f + (currentLevel * xptonextlevel * 0.1);
		}
	}
}

