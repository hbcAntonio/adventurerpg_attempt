﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GameManager : MonoBehaviour {

	/* GUI materials */
	public Material _empty_item_bg_material;
	public static Material empty_item_bg_material;
	public Material _empty_item_bg_mouseover_material;
	public static Material empty_item_bg_mouseover_material;
	public GameObject _itemPrefab;
	public static GameObject itemPrefab;

	/* Game GUI reference */
	public static GameGUI gameGUI;

	/* Item class */
	[System.Serializable]
	public class Item{
		// Item type -- USABLE, EQUIPMENT OR MISCELLANEOUS
		public enum ItemType {USABLE = 0, EQUIPMENT, MISC};

		// Item general properties
		public ItemEffect.ItemId itemId;
		public string itemName;
		public int itemQuantity;
		public ItemType itemType;
		public float itemWeight;
		public float sellValue;
		public float buyValue;
		public Material itemMaterial;
		public string itemDescription;

		public Item(){
			this.itemName = "-1"; //Item reference, unity null bug!
			this.itemMaterial = GameManager.empty_item_bg_material;
		}

		//public void setOriginalPrefab(GameObject originalPrefab){ this.originalPrefab = originalPrefab; }
		//public GameObject getOriginalPrefab() { return this.originalPrefab; }

	}

	/* Damage configurations */
	[System.Serializable]
	public class VisualDamageConfigs{
		public  Color normalDamageColor;
		public  Color criticalDamageColor;
		public  Color healDamageColor;
		public  Color specialDamageColor;
		public  Color missDamageColor;
		public  Color hitKillDamageColor;
		public  string missDamageText = "MISS";
		public  string hitKillDamageText = "HIT KILL";
		public  AnimationClip normalAnim;
		public  AnimationClip critAnim;
		public  GameObject damageText;
	}

	/* Visual damage configurations */
	public VisualDamageConfigs _damageConfigs;
	public static VisualDamageConfigs damageConfigs;

	/* Static camera reference */
	public static GameObject mainCamera;
	public static GameObject player;
	public string mainCameraTag = "MainCamera";
	public string playerTag = "Player";

	/* Items list */
	public static List<Item> itemList;

	/* Awake method */
	void Awake(){

		// Set static properies though inspector
		if (damageConfigs == null) damageConfigs = _damageConfigs;
		empty_item_bg_material = _empty_item_bg_material;
		empty_item_bg_mouseover_material = _empty_item_bg_mouseover_material;
		itemPrefab = _itemPrefab;

		// Find main camera and also get Game GUI
		if (mainCamera == null) mainCamera = GameObject.FindGameObjectWithTag (mainCameraTag);
		if (player == null) player = GameObject.FindGameObjectWithTag (playerTag);
		if (gameGUI == null) gameGUI = mainCamera.GetComponentInChildren<GameGUI> ();

		// Just initialize in case that the item list is null
		// Remember: static properties persist through levels
		if (itemList == null) itemList = new List<Item>();
	}

	void Update(){

		/* debug for checking item list
		 * if (Input.GetKeyDown(KeyCode.Y)){

			foreach (Item item in GameManager.itemList){

				// Debugging sell value
				Debug.LogWarning (item.sellValue);
			
			}

		}*/
	}

	/* Check item index by name */
	public static int getItemIndex(string itemName){
		for (int i = 0; i < GameManager.itemList.Count; ++i) {
			if (GameManager.itemList[i].itemName == itemName) return i;
		}

		// Item does not exist
		return -1;
	}

	/* Enable/Disable all renderers of an object */
	public static void setRendererActive(GameObject gobject, bool active){

		MeshRenderer[] renderers = gobject.GetComponentsInChildren<MeshRenderer> ();
		foreach (MeshRenderer meshrenderer in renderers) meshrenderer.enabled = active;

	}
	
}
