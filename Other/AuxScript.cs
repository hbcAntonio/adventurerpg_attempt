using UnityEngine;
using System.Collections;

public class AuxScript : MonoBehaviour
{
	public Material someMaterial;

	void Update(){
		if(Input.GetKeyDown(KeyCode.A)){

			GameManager.Item newItem = new GameManager.Item();
			newItem.buyValue = 0;
			newItem.sellValue = 0;
			newItem.itemQuantity = 10;
			newItem.itemWeight = 100;
			newItem.itemMaterial = someMaterial;
			newItem.itemName = "Some Item 0" + Random.Range(0, 32).ToString();
			newItem.itemDescription = "A very cool nonexistent\nitem given by AUX script!";
			newItem.itemId = ItemEffect.ItemId.NullItemId;

			Inventory.collectItem(newItem);
		}
	}
}

